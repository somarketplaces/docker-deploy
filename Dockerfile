FROM python:2.7
  
RUN pip install ansible==2.0.2 chube passlib netaddr awscli \
  && rm -rf ~/.cache/pip
